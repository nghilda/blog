class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    validates.presence_of :title
    validates.presence_of :body
end
