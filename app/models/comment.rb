class Comment < ActiveRecord::Base
    belongs_to :post
    validates.presence_of :post_id
    validates.presence_of :body
end
 